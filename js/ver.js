var sizeVal = 2;
var currentField = "[]";
var helperFunctions = {
	returnOnlyEmpty: false,
	getResponseValue: function(field, multi) {
		return field;
	},
	getParentResponseValue: function(hpiResponseOption) {
		return HpiFunctions.getResponseValue(curVars[curVars.length - 1]);
	},
	getBodyLocationsDetailedDescriptionsAsNiceString: function(hpiResponse) {
		return "body";
	},
	getProcedureBodyLocationsDetailedDescriptionsAsNiceString: function() {
		return "";
	},
	getProcedureBodyLocationsSmartDescriptionsAsNiceString: function() {
		return "{{body location}}";
	},
	getProcedureBodyLocationsExamSmartDescriptionsAsNiceString: function() {
		return "{{exam body location}}";
	},
	getOphthSmartExamDescription: function() {
		return "{{OS}}";
	},
	getDurationValue: function(field, hpiResponse) {
		return "2 years, 3 months, 1 week, and 2 days";
	},
	getProcedure: function() {
		return helperFunctions;
	},
	getProcedureName: function() {
		return " sample procecdure ";
	},
	getDiagnosis: function() {
		return helperFunctions;
	},
	getComplexity: function(hpiResponse) {
		return "complexity";
	},
	getResponseValuePC: function(field, hpiResponse) {
		return HpiFunctions.getResponseValue(field);
	},
	getResponseValueLC: function(field, hpiResponse) {
		return HpiFunctions.getResponseValue(field).toLowerCase();
	},
	setHpiNote: function(note) {
		documentnote += "This is a 20 year old female who " + note + "\n\n";
	},
	setNote: function(note) {
		subnotes[renderVar] += note;
	},
	setProcedureNote: function(note) {
		document.getElementById('procedureNote').innerHTML = note;
		console.log(note);
	},
	setBillingSummary: function(summary) {
		return "";
	},
	getHpiResponse: function() {
		return 0;
	},
	getMetadataValue: function(field) {
		return helperFunctions.getResponseValue(field);
	},
	getMetadataValues: function(field) {
		return helperFunctions.getResponseValue(field, true);
	},
	getMetadataValuesNewLines: function() {
		return "";
	},
	getAssociatedTestsDiagnosisNames: function() {
		return "";
	},
	getVisitProcedureBodyLocations: function() {
		sizeVal = 2;
		return helperFunctions;
	},
	getVisitDiagnosis: function() {
		return helperFunctions;
	},
	getDiagnosisName: function() {
		return "{{diagnosis}}";
	},
	getAdxAsNiceString: function() {
		return "{{associated diagnoses}}";
	},
	getVisitDiagnosisProcedure: function() {
		return helperFunctions;
	},
	getVeVisitProcedureMetadataResponses: function() {
		return "";
	},
	isRenderingVisitNote: function() {
		return true;
	},
	getOptionResponsesByKey: function(field) {
		currentField = field;
		return helperFunctions;
	},
	getVeProcedureMetadataOption: function() {
		return helperFunctions;
	},
	getOptionValue: function() {
		return currentField;
	},
	getTextareaValue: function() {
		return currentField;
	},
	getPatientBSA: function() {
		return 12.12;
	},
	getDateOfBirth: function() {
		return "12/12/2012";
	},
	getMrn: function() {
		return 23452150;
	},
	getAgeInMonths: function() {
		return 50;
	},
	getFullName: function() {
		return "Jon Snow";
	},
	getFirstNameLastName: function() {
		return "Jon Snow";
	},
	getSexAsString: function() {
		return "male";
	},
	getPatientWeight: function() {
		return 150;
	},
	getPatient: function() {
		return helperFunctions;
	},
	getVisit: function() {
		return helperFunctions;
	},
	getBodyLocation: function() {
		return helperFunctions;
	},
	getExamDetailDescription: function() {
		return "{{body part}}";
	},
	getBodyLocationPickerResponse: function() {
		return helperFunctions;
	},
	getPathologyLetter: function() {
		return "A";
	},
	computeDaysSinceDate: function() {
		return "17";
	},
	visitProcedureBodyLocation: function() {
		return "";
	},
	getProcedureBodyLocationsExamSimpleDescriptionsAsNiceString: function() {
		return "";
	},
	getSmartIcdCodesAsString: function() {
		return "XXX.XXXX";
	},
	getExamSimpleDescription: function() {
		return "{{location}}";
	},
	getSmartExamDescription: function() {
		return "{{location}}";
	},
	getSizeInCm: function() {
		return "123";
	},
	getSizeWithMargin: function() {
		return "654";
	},
	getPunchSizeInMm: function() {
		return "321";
	},
	getMarginInCm: function() {
		return "456";
	},
	getMarginSizeOfPunch: function() {
		return "";
	},
	getSizeWithMarginBasedOnPunch: function() {
		return "1.5";
	},
	getWoundRepairType: function() {
		return "";
	},
	hasMetadataValue: function() {
		return "";
	},
	size: function() {
		return sizeVal;
	},
	get: function(i) {
		return VeFunctions;
	}
	};

	var com = {
		m2: {
			model: {
				ProcedureBilling: function() {}
			}
		}
		};

		var VeFunctions = helperFunctions;
		var HpiFunctions = helperFunctions;

		try {
			generateNote(helperFunctions);
} catch (error) {
	document.getElementById('procedureNote').innerHTML = error;
}
